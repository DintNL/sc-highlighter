# SC Highlighter
This repository contains a number of user styles that highlight certain crews and players on [Social Club](https://socialclub.rockstargames.com/). Before you can use these styles, you will need a user style manager for your browser like [Stylus](https://add0n.com/stylus.html). Stylus is available from the [Chrome Web Store](https://chromewebstore.google.com/detail/stylus/clngdbkpkpeebahjckkjfobafhncgmne) (also for Edge and Brave) and as a [Firefox add-on](https://addons.mozilla.org/firefox/addon/styl-us/). If you are using Opera, [some extra steps may be required to install Stylus](https://github.com/openstyles/stylus/wiki/Opera,-Outdated-Stylus).


![Preview image of player in Rockstar Crew](img/player_in_rockstar_crew.png)
![Preview image of player in official Crew](img/player_in_official_crew.png)
![Preview image of banned player](img/banned_player.png)
![Preview image of player in banned crew](img/player_in_banned_crew.png)

## Available user styles
### Rockstar highlighter [(click here to view and install)](https://gitlab.com/DintNL/sc-highlighter/raw/main/rockstar.user.css)
Gives crew tags of [Rockstar Crews](https://socialclub.rockstargames.com/crews/?crewtype=rockstar&sort=membercount) a red background with white text.
### SAFE highlighter [(click here to view and install)](https://gitlab.com/DintNL/sc-highlighter/raw/main/safe.user.css)
Gives official SAFE crew tags a green background with white text and gives banned crews and banned players a red background with white text.
### HALP highlighter [(click here to view and install)](https://gitlab.com/DintNL/sc-highlighter/raw/main/halp.user.css)
Gives official HALP crew tags a green background with white text and gives banned players a red background with white text.
### Aquila highlighter [(click here to view and install)](https://gitlab.com/DintNL/sc-highlighter/raw/main/aqui.user.css)
Gives official Aquila crew tags a green background with white text.

## FAQ
### Why would I use this?
Because it allows you to easily spot official crews, banned crews and banned players on Social Club.
### Does this work in the in-game overlay?
No, it only works in your browser and only if you have a user style manager like Stylus installed.
### Are these user styles safe to use?
Yes. This is not some mod menu and it doesn't give you any super powers. The user styles only change the styling of some elements on the Social Club website. All of this happens locally in your browser. You can also see and inspect the source code of the user style before you install it.
### Which user style(s) should I install?
I would recommend installing the Rockstar highlighter, combined with the highlighter(s) of the crew(s) you're in. For example, if you're in HALP, I would recommend installing the Rockstar and HALP highlighter. If you're in both HALP and SAFE, I would recommend installing the Rockstar, HALP _and_ SAFE highlighter.
### Why is the Rockstar highlighter not included by default in the other user styles?
To avoid overlapping styles for players who are in multiple crews.
### Why are the crew tags for \<insertcrew\> not \<insertcolour\>?
While certain colours fit better with a crew's own style, I've decided to keep it simple and stick with a simple green=good and red=bad colour scheme. If you really want to change the colours, you can easily modify the colours yourself after installing the user style.
